#pragma once

#include "stdio.h"
#include "string.h"
#include "nvs_flash.h"
#include "esp_netif.h"
#include "esp_event.h"
#include "driver/gpio.h"
#include "esp_timer.h"
#include "esp_ota_ops.h"
#include "zh_espnow.h"
#include "zh_network.h"
#include "zh_ds18b20.h"
#include "zh_dht.h"
#include "zh_config.h"

#ifdef CONFIG_NETWORK_TYPE_DIRECT
#define zh_send_message(a, b, c) zh_espnow_send(a, b, c)
#define ZH_EVENT ZH_ESPNOW
#else
#define zh_send_message(a, b, c) zh_network_send(a, b, c)
#define ZH_EVENT ZH_NETWORK
#endif

#ifdef CONFIG_IDF_TARGET_ESP8266
#define ZH_CHIP_TYPE HACHT_ESP8266
#elif CONFIG_IDF_TARGET_ESP32
#define ZH_CHIP_TYPE HACHT_ESP32
#elif CONFIG_IDF_TARGET_ESP32S2
#define ZH_CHIP_TYPE HACHT_ESP32S2
#elif CONFIG_IDF_TARGET_ESP32S3
#define ZH_CHIP_TYPE HACHT_ESP32S3
#elif CONFIG_IDF_TARGET_ESP32C2
#define ZH_CHIP_TYPE HACHT_ESP32C2
#elif CONFIG_IDF_TARGET_ESP32C3
#define ZH_CHIP_TYPE HACHT_ESP32C3
#elif CONFIG_IDF_TARGET_ESP32C6
#define ZH_CHIP_TYPE HACHT_ESP32C6
#endif

#ifdef CONFIG_IDF_TARGET_ESP8266
#define ZH_CPU_FREQUENCY CONFIG_ESP8266_DEFAULT_CPU_FREQ_MHZ;
#define get_app_description() esp_ota_get_app_description()
#else
#define ZH_CPU_FREQUENCY CONFIG_ESP_DEFAULT_CPU_FREQ_MHZ;
#define get_app_description() esp_app_get_description()
#endif

#define ZH_SWITCH_KEEP_ALIVE_MESSAGE_FREQUENCY 10
#define ZH_SWITCH_ATTRIBUTES_MESSAGE_FREQUENCY 60
#define ZH_SENSOR_STATUS_MESSAGE_FREQUENCY 300
#define ZH_SENSOR_ATTRIBUTES_MESSAGE_FREQUENCY 60

#define ZH_GPIO_TASK_PRIORITY 3
#define ZH_GPIO_STACK_SIZE 2048
#define ZH_MESSAGE_TASK_PRIORITY 2
#define ZH_MESSAGE_STACK_SIZE 2048

typedef struct switch_config_t
{
    zh_switch_hardware_config_message_t hardware_config;
    zh_switch_status_message_t status;
    volatile bool gpio_processing;
    volatile bool gateway_is_available;
    uint8_t gateway_mac[ESP_NOW_ETH_ALEN];
    zh_dht_handle_t dht_handle;
    TaskHandle_t switch_attributes_message_task;
    TaskHandle_t switch_keep_alive_message_task;
    TaskHandle_t sensor_attributes_message_task;
    TaskHandle_t sensor_status_message_task;
    SemaphoreHandle_t button_pushing_semaphore;
    const esp_partition_t *update_partition;
    esp_ota_handle_t update_handle;
    uint16_t ota_message_part_number;
} switch_config_t;

void zh_load_config(switch_config_t *switch_config);
void zh_save_config(const switch_config_t *switch_config);
void zh_load_status(switch_config_t *switch_config);
void zh_save_status(const switch_config_t *switch_config);

void zh_gpio_init(switch_config_t *switch_config);
void zh_gpio_set_level(switch_config_t *switch_config);
void zh_gpio_isr_handler(void *arg);
void zh_gpio_processing_task(void *pvParameter);

void zh_send_switch_attributes_message_task(void *pvParameter);
void zh_send_switch_config_message(const switch_config_t *switch_config);
void zh_send_switch_hardware_config_message(const switch_config_t *switch_config);
void zh_send_switch_keep_alive_message_task(void *pvParameter);
void zh_send_switch_status_message(const switch_config_t *switch_config);

void zh_send_sensor_config_message(const switch_config_t *switch_config);
void zh_send_sensor_attributes_message_task(void *pvParameter);
void zh_send_sensor_status_message_task(void *pvParameter);

void zh_espnow_event_handler(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data);